const frames = [
    {
        code: 'gold-01',
        title: 'Rám 13x18 cm - zlato',
        size: '13x18 cm',
        description: 'Pozlacený rám z 22 kt zlata. Lze jej zavěsit nebo postavit (na výšku i na šířku)',
        material: 'zlato 22 kt, masivní dřevo, papír, kov',
        price: 1200,
        assets: [
            {
                code: 'asset-01',
                path: 'https://www.marcelas.cz/static/images/marcelas-theme-90-2.jpg',
                title: 'marcelas 1',
            }
        ]
    },
    {
        code: 'gold-02',
        title: 'Rám 18x24 cm - zlato',
        size: '18x24 cm',
        description: 'Pozlacený rám z 22 kt zlata. Lze jej zavěsit nebo postavit (na výšku i na šířku)',
        material: 'zlato 22 kt, masivní dřevo, papír, kov',
        price: 1800,
        assets: [
            {
                code: 'asset-02',
                path: 'https://haxe.org/img/haxe-logo-horizontal-on-dark.svg',
                title: 'marcelas 2',
            }
        ]
    },
];

module.exports = frames;