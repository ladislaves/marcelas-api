const { gql } = require('apollo-server');

const typeDefs = gql`
  type Frame {
    code: String
    title: String
    size: String
    description: String
    material: String
    price: Float
    assets: [Asset]
  }
  
  type Asset {
    code: String
    path: String
    title: String
  }

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. In this
  # case, the "books" query returns an array of zero or more Books (defined above).
  type Query {
    frames: [Frame]
  }
`;

module.exports = typeDefs;