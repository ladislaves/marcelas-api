const { ApolloServer } = require('apollo-server');
const typeDefs = require('./dsl/frames');
const frames = require('./data/frames');

const resolvers = {
    Query: {
        frames: () => frames,
    },
};

const server = new ApolloServer({ typeDefs, resolvers });

server.listen().then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
});